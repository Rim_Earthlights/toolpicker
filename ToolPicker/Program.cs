﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;

namespace ToolPicker {
    class Program {
        static string selectdir;
        static List<String> dirList;
        static Config cfg;

        #region DLL関連
        [DllImport("User32.Dll", EntryPoint = "MoveWindow")]
        public static extern void MoveWindow(
            IntPtr hWnd,     // window handle
            int x,        // x position
            int y,        // y position
            int nWitdh,   // width
            int nHeight,  // height
            int bRepaint  // redraw flag
        );
        [DllImport("User32.Dll", EntryPoint = "SetWindowPos")]
        public static extern void SetWindowPos(
            IntPtr hWnd,
            int hWndInsertAfter,
            int x,
            int y,
            int cx,
            int cy,
            uint uFlags
        );
        private const int SWP_NOSIZE = 0x0001;
        private const int SWP_NOMOVE = 0x0002;
        private const int SWP_SHOWWINDOW = 0x0040;

        private const int HWND_TOPMOST = -1;
        private const int HWND_NOTOPMOST = -2;
        #endregion

        static void Main(string[] args) {
            var handle = Process.GetCurrentProcess().MainWindowHandle;
            var screen = System.Windows.Forms.Screen.PrimaryScreen.Bounds;
            MoveWindow(handle, screen.Width - 590, screen.Height - 810, 600, 780, 1);
            SetWindowPos(handle, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
            if (Setup()) {
                int order = -1;
                bool exitflg = false;
                bool isTopMost = false;
                bool indexmode = false;
                List<String> messages = new List<string>();
                do {
                    Dictionary<Int32, KeyValuePair<String, String>> shortcuts;
                    shortcuts = MakeShortcutList(selectdir);
                    View(shortcuts, messages);
                    messages.Clear();
                    try {
                        System.Console.Write("order> ");
                        order = Console.ReadLine().Split(new char[] { ' ' }).Select(x => int.Parse(x)).First();

                    } catch {
                        messages.Add("Err: not a number.");
                        continue;
                    }
                    switch (order) {
                        case -1:
                            MoveWindow(handle, screen.Width - 590, screen.Height - 810, 600, 780, 1);
                            SetWindowPos(handle, isTopMost ? HWND_NOTOPMOST : HWND_TOPMOST, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
                            isTopMost = !isTopMost;
                            messages.Add("Info: Change Window state.");
                            break;
                        case -2:
                            cfg.Set("INDEXMODE", indexmode ? "OFF" : "ON");
                            break;
                        case -3:
                            dirList = Directory.GetDirectories("shortcuts\\", "*", SearchOption.TopDirectoryOnly).Select(x => x.Replace("shortcuts\\", "")).ToList();
                            System.Console.WriteLine("--------------------------------");
                            for (int i = 0; i < dirList.Count(); i++) {
                                System.Console.WriteLine("{0}: {1}", i, dirList[i]);
                            }
                            System.Console.WriteLine("--------------------------------");
                            System.Console.Write("select> ");
                            int sel = int.Parse(Console.ReadLine());
                            selectdir = dirList[sel];
                            cfg.Set("DIR", selectdir);
                            cfg.SaveConfig();
                            messages.Add("Info: Saved Config.");
                            break;
                        default:
                            if (shortcuts.Count() < order - 1) {
                                messages.Add("Err: Index outbounds.");
                            } else {                                                       
                                Process.Start(shortcuts[order].Value);
                            }
                            break;
                    }
                } while (!exitflg);
            }
        }

        static bool Setup() {
            if (!System.IO.File.Exists("settings.ini")) {
                using (FileStream fs = new FileStream("settings.ini", FileMode.CreateNew, FileAccess.Write)) {
                    using (StreamWriter sw = new StreamWriter(fs)) {
                        sw.WriteLine("DIR=DEFAULT");
                    }
                }
            }
            if (!Directory.Exists("shortcuts")) {
                Directory.CreateDirectory("shortcuts");
            }
            cfg = new Config("settings.ini");
            selectdir = cfg.Get("DIR");
            dirList = Directory.GetDirectories("shortcuts\\", "*", SearchOption.TopDirectoryOnly).Select(x => x.Replace("shortcuts\\", "")).ToList();
            if (dirList.Count() > 0) {
                if (!dirList.Contains(selectdir)) {
                    System.Console.WriteLine("Err: not found default dir.");
                    System.Console.WriteLine("     Please select default dir.");
                    System.Console.WriteLine("--------------------------------");
                    for (int i = 0; i < dirList.Count(); i++) {
                        System.Console.WriteLine("{0}: {1}", i, dirList[i]);
                    }
                    System.Console.WriteLine("--------------------------------");
                    System.Console.Write("select> ");
                    int sel = int.Parse(Console.ReadLine());
                    selectdir = dirList[sel];
                    cfg.Set("DIR", selectdir);
                    cfg.SaveConfig();
                }
            } else {
                System.Console.WriteLine("Err: not found directories. create dir and make shortcut in dir.");
                return false;
            }
            Console.Clear();
            return true;
        }


        static void View(Dictionary<Int32, KeyValuePair<String, String>> shortcuts, List<String> err) {
            Console.Clear();
            System.Console.WriteLine("----- Portable Launcher/仮 -----");
            System.Console.WriteLine("                Version v0.0.0.1");
            System.Console.WriteLine();
            if (err.Count() > 0) {
                foreach (var e in err) {
                    System.Console.WriteLine(e);
                }
            }
            System.Console.WriteLine("--------------------------------");
            var text = shortcuts.Select(x => String.Format("{0:D2}: {1}", x.Key, x.Value.Key));
            foreach (var t in text) {
                Console.WriteLine(t);
            }
            System.Console.WriteLine("--------------------------------");
        }

        static Dictionary<Int32, KeyValuePair<String, String>> MakeShortcutList(string mode) {
            var dic = Directory
                .GetFiles(System.IO.Path.Combine("shortcuts", mode), "*.*", SearchOption.TopDirectoryOnly)
                .ToDictionary(x => Regex.IsMatch(x, @"\.lnk$") ? System.IO.Path.GetFileNameWithoutExtension(x) : System.IO.Path.GetFileName(x), x => x);
            int i = 0;
            var result = new Dictionary<Int32, KeyValuePair<String, String>>();
            return dic.Select(x => new KeyValuePair<Int32, KeyValuePair<String, String>>(i++, new KeyValuePair<String, String>(x.Key, x.Value))).ToDictionary(x => x.Key, x => x.Value);
        }
    }
    class Config {
        private Dictionary<String, String> _cfg;
        private string _path;
        public Config(string path) {
            _path = path;
            Load();
        }

        public void Load() {
            List<String> readtext;
            readtext = File.ReadAllLines(_path).ToList();
            _cfg = readtext.Select(x => x.Split(new char[] { '=' }, 2)).ToDictionary(x => x[0], x => x[1]);
        }

        /// <summary>
        /// 設定の読込
        /// </summary>
        /// <param name="key">読み込みたい項目のキー</param>
        /// <returns>value</returns>
        public string Get(string key) {
            if (_cfg.ContainsKey(key)) {
                return _cfg
                    .Where(x => x.Key == key)
                    .Select(x => x.Value)
                    .FirstOrDefault();
            } else {
                return "";
            }
        }

        /// <summary>
        /// 設定の編集
        /// </summary>
        /// <param name="key">設定したい項目のキー</param>
        /// <param name="value">変更後の値</param>
        public void Set(string key, string value) {
            if (_cfg.ContainsKey(key)) {
                if (value.Contains("\r\n")) {
                    _cfg[key] = value.Replace("\r\n", "\\r\\n");
                } else {
                    _cfg[key] = value;
                }
            } else {
                if (value.Contains("\r\n")) {
                    _cfg.Add(key, value.Replace("\r\n", "\\r\\n"));
                } else {
                    _cfg.Add(key, value);
                }
            }
        }

        /// <summary>
        /// 設定の保存
        /// </summary>
        public void SaveConfig() {
            using (var stream = new FileStream(_path, FileMode.Create, FileAccess.Write)) {
                using (var sw = new StreamWriter(stream, Encoding.UTF8)) {
                    foreach (var item in _cfg) {
                        sw.WriteLine("{0}={1}", item.Key, item.Value);
                    }
                }
            }
        }

    }
}
